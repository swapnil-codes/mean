const express = require('express');

//middleware to handle form data
const bodyParser = require('body-parser');

const path = require('path');

const api = require('./server/routes/api');

const port = 3000;

const app = express();

//connect to angular project
app.use(express.static(path.join(__dirname, 'dist')));

//parses text as url encoded data
app.use(bodyParser.urlencoded({ extended: true }));

//parses text as json
app.use(bodyParser.json());

app.use('/api', api);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/VideoPlayer/index.html'));
});

app.listen(port,()=>{
    console.log("server is running on localhost : "+port);
})



